# Development Notes

## Ctrlr

The panel is designed for
[Ctrlr v6.0.25](https://ctrlr.org/nightly/Ctrlr-6.0.25.exe) and may
not work in lower versions.

## Knobs

The knobs and other controls are designed in
[JKnobMan v1.3.3](https://www.g200kg.com/jp/software/knobman.html).

## Font

The font used in interface is
[Gidole](https://github.com/larsenwork/Gidole).
