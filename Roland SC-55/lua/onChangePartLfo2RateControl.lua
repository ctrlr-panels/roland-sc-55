--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartLfo2RateControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  value = fitInRange(value, -10.0, 10.0)

  -- Setting correct value into label
  mods.setValue(label, string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-lfo2-rate-mod",
    "part-lfo2-rate-bend",
    "part-lfo2-rate-cc1",
    "part-lfo2-rate-cc2",
    "part-lfo2-rate-caf",
    "part-lfo2-rate-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.ceil((value + 10) * 127 / 20)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 7,
    midiValue,
  }
  midi.sendGSSysex(data)

end