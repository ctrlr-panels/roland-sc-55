--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

TARGET_LEN = 16

onChangePatchName = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local newValue = newContent

  -- string length validation
  if newValue:len() > TARGET_LEN then
    newValue = newValue:sub(1, TARGET_LEN)
  end

  label:setComponentText(newValue)

  -- pad name to the length of 16
  if newValue:len() < TARGET_LEN then
    newValue = string.format("%-16s", newValue)
  end

  local data = {0x40, 0x01, 0x00, newValue:byte(1, TARGET_LEN)}
  midi.sendGSSysex(data)

end