-- Module to work with MIDI

local math = require('math')

local global = _G
local ipairs = global.ipairs
local panel = global.panel
local table = global.table

local CtrlrMidiMessage = global.CtrlrMidiMessage

module("midi")


-- 
-- Calculates Roland checksum of an array of values
-- 
function getChecksum(--[[ Array --]] values)
  local acc = 0
  for _, value in ipairs(values) do
    acc = acc + value
  end

  local remainder = math.fmod(acc, 128)

  return 128 - remainder
end


-- 
-- Sends GStandard (GS) sysex message with the
-- given payload data.
-- The payload data must consist of:
--  * Address (3 bytes)
--  * Data (the size depends on the parameter)
-- 
function sendGSSysex(--[[ Array --]] values)
  local data = {
    0xf0,                        -- Exclusive status
    0x41,                        -- Manufacturer ID (Roland)
    panel:getGlobalVariable(0),  -- Device ID (16 by default)
    0x42,                        -- Model ID (GStandard)
    0x12,                        -- Command (DT1 / Data set 1)
  }

  for i=1, #values do
    data[#data + 1] = values[i]
  end
  
  -- Roland checksum
  local checksum = getChecksum(values)
  table.insert(data, checksum)

  -- EOX
  table.insert(data, 0xF7)
  
  local msg = CtrlrMidiMessage(data)
  panel:sendMidiMessageNow(msg)
end