-- Work with modulators

local global = _G
local class_info = global.class_info
local log = global.log
local pairs = global.pairs
local panel = global.panel
local table = global.table
local tostring = global.tostring

module("mods")


-- 
-- Search modulator by its name.
-- A shortcut for `getModulatorByName()`
-- 
function get(--[[ string --]] name)
  return panel:getModulatorByName(name)
end


-- Searches multiple modulators by their names.
function getMany(names)
  result = {}
  for _, n in pairs(names) do
    mod = get(n)
    table.insert(result, mod)
  end
  return result
end


-- 
-- Gets modulator value
-- 
-- @mod    Modulator or its name
-- 
function getValue(mod)

  mod = _ensureModulator(mod)
  if mod == nil then
    log("[ERROR] Cannot get the value of the empty modulator.")
  end

  local modType = ""

  if isModulator(mod) then
    modType = class_info(mod:getComponent()).name
  else
    log("Not a modulator passed.")
    return
  end

  if (
    modType == "CtrlrSlider"
    or modType == "CtrlrToggleButton"
    or modType == "CtrlrCombo"
  ) then
    return mod:getValue()
  elseif (
    modType == "CtrlrLabel"
    or modType == "CtrlrLCDLabel"
  ) then
    return mod:getComponent():getText():unquoted()
  end
end


-- 
-- Sets the given value to modulator to its default value.
-- 
-- @name    Modulator or its name
-- 
function setValue(mod, value)

  mod = _ensureModulator(mod)
  if mod == nil then
    log("[ERROR] Cannot set the value of the empty modulator.")
  end

  local modType = ""

  if isModulator(mod) then
    modType = class_info(mod:getComponent()).name
  else
    log("Not a modulator passed.")
    return
  end

  local modName = mod:getName():unquoted()

  -- Eliminating infinite or deep recursion
  local isRecursiveCall = global.modsBeingSet[modName] ~= nil
  if isRecursiveCall then
    return
  end
  
  global.modsBeingSet[modName] = true

  if (
    modType == "CtrlrSlider"
    or modType == "CtrlrToggleButton"
    or modType == "CtrlrCombo"
  ) then
    mod:setValue(value, false, true)
  elseif (
    modType == "CtrlrLabel"
    or modType == "CtrlrLCDLabel"
  ) then
    mod:getComponent():setText(tostring(value))
  end

  global.modsBeingSet[modName] = nil

end


-- Returns the modulator wherever the mod or its name passed.
function _ensureModulator(mod)

  local modType = class_info(mod).name
  
  -- If name passed instead of modulator then get the modulator
  if modType == "string" then
    mod = get(mod)
  elseif modType == "String" then
    mod = get(mod:unquoted())
  elseif isLabel(mod) then
    mod = mod:getOwner()
  end

  return mod

end


-- Is the object a modulator?
function isModulator(obj)
  return class_info(obj).name == "CtrlrModulator"
end


function isLabel(obj)
  local modType = class_info(obj).name
  return modType == "CtrlrLabel" or modType == "CtrlrLCDLabel"
end


-- Reset modulator value to its default
function reset(--[[ string --]] name)
	local modulator = get(name)
    if modulator then
      local defaultValue = modulator:getComponent():getPropertyInt("uiSliderDoubleClickValue")
      modulator:setModulatorValue(defaultValue, false, false, false)
    else
      log("[ERROR] Couldn't find the modulator: " .. name)
    end
end


-- 
-- Reads modulators values to table.
-- 
-- @defaultMods  Mapping table of mod names and their default values
-- 
function readToTable(defaultMods)
  local result = {}
  for modName, default in pairs(defaultMods) do
    local mod = get(modName)
    if mod then
      result[modName] = getValue(mod)
    else
      result[modName] = default
    end
  end
  return result
end
