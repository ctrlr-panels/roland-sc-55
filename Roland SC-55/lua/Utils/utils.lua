-- Utility functions
-- 
-- Here we collect the utility functions that not bound
-- to any Ctrlr Panel and can be reused in others without
-- any edition.


-- 
-- `log()`
-- console() sometimes gets dumb and doesn't work
-- this one is a bit more robust
-- 
function log(value)
  console(String(tostring(value)))
end


function log_table(value)

  if not _G.type == "table" then
    log("Not a table")
  end
  
  log("----------")
  
  for k, v in pairs(value) do
    log(tostring(k) .. ": " .. tostring(v))
  end

end


-- Tries to convert to double. Returns default if no luck.
function convertToNumber(--[[ string --]] value, --[[ number --]] default)
  local retValue = tonumber(value)
  if retValue ~= nil then
    return retValue
  else
    return default
  end
end


function fitInRange(value, min, max)
  if value < min then
    value = min
  elseif value > max then
    value = max
  end
  return value
end


-- 
-- Deepcopy
-- Original: http://lua-users.org/wiki/CopyTable
-- 
function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
