-- Extensions of standard Lua library
-- (which is too minimalistic)


-- True if string starts with other string
function string.startswith(str, value)
   return string.sub(str, 1, string.len(value)) == value
end


-- 
-- Length of the table
-- Lua Hash (#) len may not work for some mappings.
-- 
function table.len(tab)
  local i = 0
  for _ in pairs(tab) do
    i = i + 1
  end
  return i
end


-- 
-- Extends the table `tab1` with another one (`tab2`)
-- 
-- It can extend with both simple 'array' table or with
-- the hash table. It can also mix both types of tables.
-- 
function table.extend(tab1, tab2)
  local i = 1
  for k, v in pairs(tab2) do
    if _G.type(k) == "number" and k == i then
      table.insert(tab1, v)
    else
      tab1[k] = v
    end
    i = i + 1
  end
end


-- Clears the Lua table
function table.clear(tab)
  for key in pairs (tab) do
    tab[key] = nil
  end
end
