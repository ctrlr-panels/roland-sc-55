--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartLfo1RateControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  value = fitInRange(value, -10.0, 10.0)

  -- Setting correct value into label
  mods.setValue(label, string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-lfo1-rate-mod",
    "part-lfo1-rate-bend",
    "part-lfo1-rate-cc1",
    "part-lfo1-rate-cc2",
    "part-lfo1-rate-caf",
    "part-lfo1-rate-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.ceil((value + 10) * 127 / 20)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 3,
    midiValue,
  }
  midi.sendGSSysex(data)

end