--
-- Called when the CC1 Controller Number is changed
-- @label       modulator (label)
-- @newContent  a new value (string)
--

onChangePartPitchFine = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  if value < -12 then
    value = -12
  elseif value > 12 then
    value = 12
  end

  -- Setting correct value into label
  label:setComponentText(string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua("part-pitch-fine") then
    return
  end

  local normalizedValue = BigInteger(value * 10 + 128)

  local data = {
    0x40,
    panel:getGlobalVariable(1),
    0x17,
    normalizedValue:getBitRangeAsInt(4,4),  -- LSB/1
    normalizedValue:getBitRangeAsInt(0,4),  -- LSB/2
  }
  midi.sendGSSysex(data)
end