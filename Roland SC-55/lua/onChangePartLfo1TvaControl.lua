--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartLfo1TvaControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  value = fitInRange(value, 0.0, 100.0)

  -- Setting correct value into label
  mods.setValue(label, string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-lfo1-tva-mod",
    "part-lfo1-tva-bend",
    "part-lfo1-tva-cc1",
    "part-lfo1-tva-cc2",
    "part-lfo1-tva-caf",
    "part-lfo1-tva-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.ceil(value * 127 / 100.0)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 6,
    midiValue,
  }
  midi.sendGSSysex(data)

end