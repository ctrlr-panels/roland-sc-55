--
-- Called when a global Chorus Type changes
--
-- In SC-55 all the reverb parameters are being reset after type change
-- The `Character` setting is being reset to the type of Reverb also
-- 
-- @mod      reverb modulator
-- @value    new numeric value of the modulator
-- 

REVERB_MODULATOR_NAMES = {
  "reverb-pre-lpf",
  "reverb-time",
  "reverb-delay-fbk",
  "reverb-send-to-chorus",
}

onChangeReverbType = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)
  -- resetting reverb parameters
  for i = 1, #REVERB_MODULATOR_NAMES do
    mods.reset(REVERB_MODULATOR_NAMES[i])
  end

  -- setting default 'character' value
  local character_mod = mods.get("reverb-character")
  if character_mod then
    character_mod:setModulatorValue(value, false, false, false)
  end
  
end