--
-- Called when a click on some numbered Part is made
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--
onClickRegularPart = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)

  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  -- Highlighting the button
  mod:getComponent():setValue(1, false)

  local newPart = parts.btn.getPartNo(mod)
  if newPart ~= currentPart then
    partConfigs[currentPart] = gatherCurrentPartConfig()
  end

  parts.activate(newPart)

end


function gatherCurrentPartConfig()
  local currentPartType = parttypes.get(currentPart)
  return partcfg.gather(currentPartType)
end
