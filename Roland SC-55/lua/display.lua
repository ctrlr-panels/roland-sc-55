function display(txt)
  local mod = mods.get("modulator-1")
  mod:getComponent():setProperty("uiToggleButtonText", tostring(txt), false)
end