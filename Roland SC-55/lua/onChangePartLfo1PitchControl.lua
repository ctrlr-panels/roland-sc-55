--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartLfo1PitchControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value)
  value = fitInRange(value, 0, 600)

  -- Setting correct value into label
  mods.setValue(label, string.format("%d", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-lfo1-pitch-mod",
    "part-lfo1-pitch-bend",
    "part-lfo1-pitch-cc1",
    "part-lfo1-pitch-cc2",
    "part-lfo1-pitch-caf",
    "part-lfo1-pitch-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.ceil(value * 127 / 600)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 4,
    midiValue,
  }
  midi.sendGSSysex(data)

end