--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartAmpControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  value = fitInRange(value, -100.0, 100.0)

  -- Setting correct value into label
  mods.setValue(label, string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-amp-ctrl-mod",
    "part-amp-ctrl-bend",
    "part-amp-ctrl-cc1",
    "part-amp-ctrl-cc2",
    "part-amp-ctrl-caf",
    "part-amp-ctrl-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.ceil((value + 100) * 127 / 200)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 2,
    midiValue,
  }
  midi.sendGSSysex(data)

end