--
-- Called when a modulator value changes
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--
onChangePartGroup = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)

  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  -- Getting and setting current group number
  partConfigs[currentPart].group = value + 1 

  -- Saving current bank instruments list into memory
  currentGroup = instruments.getList(value + 1, 1)  -- TODO: replace model no (mk I by default)

  -- Setting instrument combobox values list and value
  local combo = mods.get("part-instrument")
  local instrNo = mods.getValue(combo)
  combo:getComponent():setProperty(
    "uiComboContent", table.concat(currentGroup, "\n"), false
  )
  if table.len(currentGroup) >= instrNo then
    combo:getComponent():setValue(instrNo, false)  -- because `mods.setValue()` doesn't work here!
  end
  
end