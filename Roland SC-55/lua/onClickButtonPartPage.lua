--
-- Called when a modulator value changes
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--


onClickButtonPartPage = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local buttonName = mod:getName():unquoted()
  local layerName = partPageButtonToLayerMap[buttonName]
  partpages.activate(layerName)

  partpages.btn.activate(mod)
end
