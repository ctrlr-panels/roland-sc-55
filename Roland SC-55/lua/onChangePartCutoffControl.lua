--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartCutoffControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value)
  value = fitInRange(value, -9600, 9600)
  value = math.floor((value + 9600) / 150) * 150 - 9600

  -- Setting correct value into label
  mods.setValue(label, string.format("%d", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-cutoff-ctrl-mod",
    "part-cutoff-ctrl-bend",
    "part-cutoff-ctrl-cc1",
    "part-cutoff-ctrl-cc2",
    "part-cutoff-ctrl-caf",
    "part-cutoff-ctrl-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)

  local midiValue = math.floor(value / 150) + 64
  midiValue = fitInRange(value, 0, 127)

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 1,
    midiValue,
  }
  midi.sendGSSysex(data)

end