--
-- Called when the Master Tune is changed
-- @label
-- @newContent    a string that the label now contains
--

onChangeMasterTune = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)

  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value * 10) / 10
  if value < -100 then
    value = -100
  elseif value > 100 then
    value = 100
  end

  -- Setting correct value into label
  label:setComponentText(string.format("%.1f", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua("master-tune") then
    return
  end

  local normalizedValue = BigInteger(value * 10 + 1024)

  local data = {
    0x40, 0x00, 0x00,
    normalizedValue:getBitRangeAsInt(12,4), -- MSB/1
    normalizedValue:getBitRangeAsInt(8,4),  -- MSB/2
    normalizedValue:getBitRangeAsInt(4,4),  -- LSB/1
    normalizedValue:getBitRangeAsInt(0,4),  -- LSB/2
  }
  midi.sendGSSysex(data)

end