function isPanelReady()
  return not panel:getBootstrapState() and isPanelInitialized
end

-- Does callback needs to be muted?
function isTriggeredFromLua(names)

  -- if `names` is string then check against it
  if _G.type(names) ~= "table" then
    return modsBeingSet[names] ~= nil
  end
  
  -- If `names` is table then iterate and check every name
  for _, name in ipairs(names) do
    if modsBeingSet[name] ~= nil then
      return true
    end
  end
  return false
end
