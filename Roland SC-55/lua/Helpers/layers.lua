local canvas = panel:getCanvas()

module("layers", package.seeall)

-- Classes
top = {}
partpage = {}
parttype = {}


local topLevelLayers = {
  "General",
  "Part",
}


top.showGeneral = function()
  canvas:getLayerByName("General"):setVisible(true)
  canvas:getLayerByName("Part"):setVisible(false)

  canvas:getLayerByName("Normal-Backdrop"):setVisible(false)
  canvas:getLayerByName("Drums-Backdrop"):setVisible(false)

  for _, layer in pairs(partPageButtonToLayerMap) do
    canvas:getLayerByName(layer):setVisible(false)
  end
end


top.showPart = function()
  canvas:getLayerByName("Part"):setVisible(true)
  canvas:getLayerByName("General"):setVisible(false)

  partpages.activate(partpages.getActive())
end


-- Hides drum layers and shows normal ones
parttype.showNormal = function(activePartPage)
  canvas:getLayerByName("Drums-Backdrop"):setVisible(false)
  canvas:getLayerByName("Normal-Backdrop"):setVisible(true)
  if activePartPage == "Drums-General" then
    partpages.activate("Aftertouch")
    --partpages.show("Aftertouch")
  end
end


-- Hides unnecessary 'normal' layers and shows drums
parttype.showMap = function()
  canvas:getLayerByName("Normal-Backdrop"):setVisible(false)
  canvas:getLayerByName("Drums-Backdrop"):setVisible(true)
end


partpage.show = function(selected)
  for _, layer in pairs(partPageButtonToLayerMap) do
    canvas:getLayerByName(layer):setVisible(false)
  end
  canvas:getLayerByName(selected):setVisible(true)
end

