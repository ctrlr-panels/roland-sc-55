-- Setting the global variables on startup.
--
-- Constants
-- ---------
-- DRUM_PART_NUM          Default drum part number
-- MAX_POLYPHONY          Maximum voices to assign for reserve total
-- MAX_DRUM_PARTS         Maximum amount of drum parts (2 for SC-55)
-- PARTS_COUNT            Number of parts total (16 for SC-55)
-- 
-- Variables
-- ---------
-- activePartPage         Name of the layer which must be shown at the moment
-- currentPart            Current part shown on Panel
-- drumParts              The table of assigned drum parts
-- isPanelInitialized     Set to `true` when the panel is loaded, otherwise `false`
-- modsBeingSet           Stores the modulator/s name/s during their processing
-- partConfigs            Array that stores the settings by each part
-- 
-- SEE ALSO the `initModulators` module, where the REST of the GLOBALS are initialized.

DRUM_PART_NUM = 10
PARTS_COUNT = 16
MAX_POLYPHONY = 24
MAX_DRUM_PARTS = 2

-- This variable will be set to true after all the modulators
-- are initialized. This prevents false triggers of the
-- callbacks and thus phantom midi messages.
isPanelInitialized = false

-- Ctrlr has no real 'On click' event, but only a 'On change'
-- which serves bad if you change the values programmatically.
-- In theory, there is `source` parameter, but its behavior
-- isn't stable and such controls as 'CtrlrLabel' have no
-- source parameter in the signature of the callback.
-- The following map is a kludge to overcome the issue.
-- Here we'll set the list of modulators that being set right
-- now from code (see `mods.setValue` helper function
-- and `isTriggeredFromLua` helper function).
modsBeingSet = {}

function initGlobals()

  -- Set current part
  if currentPart == nil then
    parts.setGlobal(1)
  end

  -- Init module constants
  instruments.init()

  -- If no state saved (nil) or json decode failed
  -- then we need to reinitialize part configs
  -- to avoid crashes
  if _G.type(partConfigs) ~= "table" then
    partConfigs = {}
  end

  -- Setting default part configurations if nothing saved
  if table.len(partConfigs) < PARTS_COUNT then
    table.clear(partConfigs)

    for i=1, PARTS_COUNT do
      partConfigs[i] = partcfg.getDefault()
      
      partConfigs[i].mods["part-midi-ch"] = i - 1

      if i == DRUM_PART_NUM then
        partConfigs[i].partType = 1  -- assign MAP 1 by default
        partConfigs[i].mods["part-assign-mode"] = 0
      end
    end
    
  end

  -- Setting Drum part settings
  drumParts = {}
  for i, config in pairs(partConfigs) do
    if config.partType and config.partType > 0 then
      drumParts[config.partType] = i
    end
  end

end