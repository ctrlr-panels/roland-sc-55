-- Helpers for part pages (layers that make up the part's
-- appearance).

module("partpages", package.seeall)

btn = {}


function getActive()
  if activePartPage == nil then
    return "Basic"
  else
    return activePartPage
  end
end


function activate(selected)
  setGlobal(selected)
  
  -- Activate the part page layer
  local button = btn.getByPage(activePartPage)
  btn.activate(button)

  layers.partpage.show(selected)
end


-- Sets current part page globals
function setGlobal(page)
  _G.activePartPage = page
  partConfigs[currentPart]["activePage"] = page
end


-- Resets all the other buttons in group
btn.activate = function(selectedButton)
  for _, button in pairs(partPageButtons) do
    if button ~= selectedButton then
      button:getComponent():setValue(0, false)
    end
  end
  selectedButton:getComponent():setValue(1, false)
end


btn.getByPage = function(page)
  local buttonName = _G.partPageLayerToButtonMap[activePartPage]
  return partPageButtons[buttonName]
end
