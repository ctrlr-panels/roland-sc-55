

module("instruments", package.seeall)

local mk1 = {}
local mk2 = {}


function chooseModel(modelNo)
  local model = mk1
  if modelNo == 2 then
    model = mk2
  end
  return model
end


function getGroupNames(modelNo)
  local result = {}
  local model = chooseModel(modelNo)
  for group in model.groups do
    table.insert(result, group.name)
  end
  return result
end


function getList(groupId, modelNo)

  local model = chooseModel(modelNo)
  
  result = {}
  for _, sound in pairs(model.groups[groupId]["sounds"]) do

    local bank, instrId, name, voices = unpack(sound)

    local bankRepr = ""
    if bank ~= 0 and bank ~= 127 then
        bankRepr = string.format("%03d", bank) .. "/"
    end
    local repr = bankRepr .. string.format("%03d", instrId) .. " " .. name

    table.insert(result, repr)

  end
  
  return result
  
end


function init()

  -- The sounds come as tables in following format:
  --   * Variation (bank) index
  --   * Instrument index
  --   * Instrument displayed name
  --   * Number of voices taken
  mk1.groups = {
    {
      name = "Capital",
      sounds = {
        -- Piano
        {0, 1, "Piano 1", 1},
        {0, 2, "Piano 2", 1},
        {0, 3, "Piano 3", 1},
        {0, 4, "Honky-Tonk", 2},
        {0, 5, "Electric Piano 1", 1},
        {0, 6, "Electric Piano 2", 1},
        {0, 7, "Harpsichord", 1},
        {0, 8, "Clavinet", 1},
        -- Chromatic percussion
        {0, 9, "Celesta", 1},
        {0, 10, "Glockenspiel", 1},
        {0, 11, "Music Box", 1},
        {0, 12, "Vibraphone", 1},
        {0, 13, "Marimba", 1},
        {0, 14, "Xylophone", 1},
        {0, 15, "Tubular Bell", 1},
        {0, 16, "Santur", 1},
        -- Organ
        {0, 17, "Organ 1", 1},
        {0, 18, "Organ 2", 1},
        {0, 19, "Organ 3", 1},
        {0, 20, "Church Organ 1", 1},
        {0, 21, "Reed Organ", 1},
        {0, 22, "Accordion Fr", 2},
        {0, 23, "Harmonica", 1},
        {0, 24, "Bandoneon", 2},
        -- Guitar
        {0, 25, "Nylon-string Guitar", 1},
        {0, 26, "Steel-string Guitar", 1},
        {0, 27, "Jazz Guitar", 1},
        {0, 28, "Clean Guitar", 1},
        {0, 29, "Muted Guitar", 1},
        {0, 30, "Overdrive Guitar", 1},
        {0, 31, "Distortion Guitar", 1},
        {0, 32, "Guitar Harmonics", 1},
        -- Bass
        {0, 33, "Acoustic Bass", 1},
        {0, 34, "Fingered Bass", 1},
        {0, 35, "Picked Bass", 1},
        {0, 36, "Fretless Bass", 1},
        {0, 37, "Slap Bass 1", 1},
        {0, 38, "Slap Bass 2", 1},
        {0, 39, "Synth Bass 1", 1},
        {0, 40, "Synth Bass 2", 1},
        -- Strings / orchestra
        {0, 41, "Violin", 1},
        {0, 42, "Viola", 1},
        {0, 43, "Cello", 1},
        {0, 44, "Contrabass", 1},
        {0, 45, "Tremolo Strings", 1},
        {0, 46, "Pizzicato Strings", 1},
        {0, 47, "Harp", 1},
        {0, 48, "Timpani", 1},
        -- Ensemble
        {0, 49, "Strings", 1},
        {0, 50, "Slow Strings", 1},
        {0, 51, "Synth Strings 1", 1},
        {0, 52, "Synth Strings 2", 2},
        {0, 53, "Choir Aahs", 1},
        {0, 54, "Voice Oohs", 1},
        {0, 55, "SynVox", 1},
        {0, 56, "Orchestra Hit", 2},
        -- Brass
        {0, 57, "Trumpet", 1},
        {0, 58, "Trombone", 1},
        {0, 59, "Tuba", 1},
        {0, 60, "Muted Trumpet", 1},
        {0, 61, "French Horn", 2},
        {0, 62, "Brass 1", 1},
        {0, 63, "Synth Brass 1", 2},
        {0, 64, "Synth Brass 2", 2},
        -- Lead
        {0, 65, "Soprano Sax", 1},
        {0, 66, "Alto Sax", 1},
        {0, 67, "Tenor Sax", 1},
        {0, 68, "Baritone Sax", 1},
        {0, 69, "Oboe", 1},
        {0, 70, "English Horn", 1},
        {0, 71, "Bassoon", 1},
        {0, 72, "Clarinet", 1},
        -- Pipe
        {0, 73, "Piccolo", 1},
        {0, 74, "Flute", 1},
        {0, 75, "Recorder", 1},
        {0, 76, "Pan Flute", 1},
        {0, 77, "Bottle Blow", 2},
        {0, 78, "Shakuhachi", 2},
        {0, 79, "Whistle", 1},
        {0, 80, "Ocarina", 1},
        -- Synth lead
        {0, 81, "Square Wave", 2},
        {0, 82, "Saw Wave", 2},
        {0, 83, "Synth Calliope", 2},
        {0, 84, "Chiffer Lead", 2},
        {0, 85, "Charang", 2},
        {0, 86, "Solo Vox", 2},
        {0, 87, "5th Saw Wave", 2},
        {0, 88, "Bass & Lead", 2},
        -- Synth pad etc.
        {0, 89, "Fantasia", 2},
        {0, 90, "Warm Pad", 1},
        {0, 91, "Polysynth", 2},
        {0, 92, "Space Voice", 1},
        {0, 93, "Bowed Glass", 2},
        {0, 94, "Metal Pad", 2},
        {0, 95, "Halo Pad", 2},
        {0, 96, "Sweep Pad", 1},
        -- Synth SFX
        {0, 97, "Ice Rain", 2},
        {0, 98, "Soundtrack", 2},
        {0, 99, "Crystal", 2},
        {0, 100, "Atmosphere", 2},
        {0, 101, "Brightness", 2},
        {0, 102, "Goblin", 2},
        {0, 103, "Echo Drops", 1},
        {0, 104, "Star Theme", 2},
        -- Ethnic
        {0, 105, "Sitar", 1},
        {0, 106, "Banjo", 1},
        {0, 107, "Shamisen", 1},
        {0, 108, "Koto", 1},
        {0, 109, "Kalimba", 1},
        {0, 110, "Bag Pipe", 1},
        {0, 111, "Fiddle", 1},
        {0, 112, "Shanai", 1},
        -- Percussive
        {0, 113, "Tinkle Bell", 1},
        {0, 114, "Agogo", 1},
        {0, 115, "Steel Drums", 1},
        {0, 116, "Woodblock", 1},
        {0, 117, "Taiko", 1},
        {0, 118, "Melodic Tom 1", 1},
        {0, 119, "Synth Drum", 1},
        {0, 120, "Reverse Cymbals", 2},
        -- SFX
        {0, 121, "Guitar Fret Noise", 1},
        {0, 122, "Flute Keyclick", 1},
        {0, 123, "Seashore", 1},
        {0, 124, "Bird", 2},
        {0, 125, "Telephone 1", 1},
        {0, 126, "Helicopter", 1},
        {0, 127, "Applause", 2},
        {0, 128, "Gun Shot", 1},
      }
    },
    {
      name = "GS Variations",
      sounds = {
        {8, 5, "Detuned EP 1", 2},
        {8, 6, "Detuned EP 2", 2},
        {8, 7, "Coupled Hps.", 2},
        {8, 15, "Church Bell", 1},
        {8, 17, "Detuned Organ 1", 2},
        {8, 18, "Detuned Organ 2", 2},
        {8, 20, "Church Organ 2", 2},
        {8, 22, "Accordion It", 2},
        {8, 25, "Ukulele", 1},
        {8, 26, "12-string Guitar", 2},
        {16, 26, "Mandolin", 1},
        {8, 27, "Hawaiian Guitar", 1},
        {8, 28, "Chorus Guitar", 2},
        {8, 29, "Funk Guitar", 1},
        {8, 31, "Feedback Guitar", 2},
        {8, 32, "Guitar Feedback", 1},
        {8, 39, "Synth Bass 3", 1},
        {8, 40, "Synth Bass 4", 2},
        {8, 49, "Orchestra", 2},
        {8, 51, "Synth Strings 3", 2},
        {8, 62, "Brass 2", 2},
        {8, 63, "Synth Brass 3", 2},
        {8, 64, "Synth Brass 4", 1},
        {8, 108, "Taisho Koto", 2},
        {8, 116, "Castanets", 1},
        {8, 117, "Concert BD", 1},
        {8, 118, "Melodic Tom 2", 1},
        {8, 119, "808 Tom", 1},
      }
    },
    {
      name = "GS SFX",
      sounds = {
        {1, 121, "Guitar Cut Noise", 1},
        {2, 121, "String Slap", 1},
        {1, 123, "Rain", 2},
        {2, 123, "Thunder", 1},
        {3, 123, "Wind", 1},
        {4, 123, "Stream", 2},
        {5, 123, "Bubble", 2},
        {1, 124, "Dog", 1},
        {2, 124, "Horse", 1},
        {1, 125, "Telephone 2", 1},
        {2, 125, "Door Creaking", 1},
        {3, 125, "Door", 1},
        {4, 125, "Scratch", 1},
        {5, 125, "Windchime", 2},
        {1, 126, "Car - Engine", 1},
        {2, 126, "Car - Stop", 1},
        {3, 126, "Car - Pass", 1},
        {4, 126, "Car - Crash", 2},
        {5, 126, "Siren", 1},
        {6, 126, "Train", 1},
        {7, 126, "Jetplane", 2},
        {8, 126, "Starship", 2},
        {9, 126, "Burst Noise", 2},
        {1, 127, "Laughing", 1},
        {2, 127, "Screaming", 1},
        {3, 127, "Punch", 1},
        {4, 127, "Heart Beat", 1},
        {5, 127, "Foot Step", 1},
        {1, 128, "Machinegun", 1},
        {2, 128, "Lasergun", 1},
        {3, 128, "Explosion", 2},
      }
    },
    {
      name = "MT-32",
      sounds = {
        {127, 1, "Acou Piano 1", 1},
        {127, 2, "Acou Piano 2", 1},
        {127, 3, "Acou Piano 3", 1},
        {127, 4, "Elec Piano 1", 1},
        {127, 5, "Elec Piano 2", 1},
        {127, 6, "Elec Piano 3", 1},
        {127, 7, "Elec Piano 4", 1},
        {127, 8, "Honkytonk", 2},
        {127, 9, "Elec Org 1", 1},
        {127, 10, "Elec Org 2", 2},
        {127, 11, "Elec Org 3", 1},
        {127, 12, "Elec Org 4", 1},
        {127, 13, "Pipe Org 1", 2},
        {127, 14, "Pipe Org 2", 2},
        {127, 15, "Pipe Org 3", 2},
        {127, 16, "Accordion", 2},
        {127, 17, "Harpsi 1", 1},
        {127, 18, "Harpsi 2", 2},
        {127, 19, "Harpsi 3", 1 },
        {127, 20, "Clavi 1", 1},
        {127, 21, "Clavi 2", 1},
        {127, 22, "Clavi 3", 1},
        {127, 23, "Celesta 1", 1},
        {127, 24, "Celesta 2", 1},
        {127, 25, "Syn Brass 1", 2},
        {127, 26, "Syn Brass 2", 2},
        {127, 27, "Syn Brass 3", 2},
        {127, 28, "Syn Brass 4", 2},
        {127, 29, "Syn Bass 1", 1},
        {127, 30, "Syn Bass 2", 2},
        {127, 31, "Syn Bass 3", 2},
        {127, 32, "Syn Bass 4", 1},
        {127, 33, "Fantasy", 2},
        {127, 34, "Harmo Pan", 2},
        {127, 35, "Chorale", 1},
        {127, 36, "Glasses", 2},
        {127, 37, "Soundtrack", 2},
        {127, 38, "Atmosphere", 2},
        {127, 39, "Warm Bell", 2},
        {127, 40, "Funny Vox", 1},
        {127, 41, "Echo Bell", 2},
        {127, 42, "Ice Rain", 2},
        {127, 43, "Oboe 2001", 2},
        {127, 44, "Echo Pan", 2},
        {127, 45, "Doctor Solo", 2},
        {127, 46, "School Daze", 1},
        {127, 47, "Bellsinger", 1},
        {127, 48, "Square Wave", 2},
        {127, 49, "Str Sect 1", 1},
        {127, 50, "Str Sect 2", 1},
        {127, 51, "Str Sect 3", 1},
        {127, 52, "Pizzicato", 1},
        {127, 53, "Violin 1", 1},
        {127, 54, "Violin 2", 1},
        {127, 55, "Cello 1", 1},
        {127, 56, "Cello 2", 1},
        {127, 57, "Contrabass", 1},
        {127, 58, "Harp 1", 1},
        {127, 59, "Harp 2", 1},
        {127, 60, "Guitar 1", 1},
        {127, 61, "Guitar 2", 1},
        {127, 62, "Elec Gtr 1", 1},
        {127, 63, "Elec Gtr 2", 1},
        {127, 64, "Sitar", 2},
        {127, 65, "Acou Bass 1", 1},
        {127, 66, "Acou Bass 2", 1},
        {127, 67, "Elec Bass 1", 1},
        {127, 68, "Elec Bass 2", 1},
        {127, 69, "Slap Bass 1", 1},
        {127, 70, "Slap Bass 2", 1},
        {127, 71, "Fretless 1", 1},
        {127, 72, "Fretless 2", 1},
        {127, 73, "Flute 1", 1},
        {127, 74, "Flute 2", 1},
        {127, 75, "Piccolo 1", 1},
        {127, 76, "Piccolo 2", 2},
        {127, 77, "Recorder", 1},
        {127, 78, "Pan Pipes", 1},
        {127, 79, "Sax 1", 1},
        {127, 80, "Sax 2", 1},
        {127, 81, "Sax 3", 1},
        {127, 82, "Sax 4", 1},
        {127, 83, "Clarinet 1", 1},
        {127, 84, "Clarinet 2", 1},
        {127, 85, "Oboe", 1},
        {127, 86, "Engl Horn", 1},
        {127, 87, "Bassoon", 1},
        {127, 88, "Harmonica", 1},
        {127, 89, "Trumpet 1", 1},
        {127, 90, "Trumpet 2", 1},
        {127, 91, "Trombone 1", 1},
        {127, 92, "Trombone 2", 1},
        {127, 93, "Fr Horn 1", 1},
        {127, 94, "Fr Horn 2", 1},
        {127, 95, "Tuba", 1},
        {127, 96, "Brs Sect 1", 1},
        {127, 97, "Brs Sect 2", 2},
        {127, 98, "Vibe 1", 1},
        {127, 99, "Vibe 2", 1},
        {127, 100, "Syn Mallet", 1},
        {127, 101, "Windbell", 2},
        {127, 102, "Glock", 1},
        {127, 103, "Tube Bell", 1},
        {127, 104, "Xylophone", 1},
        {127, 105, "Marimba", 1},
        {127, 106, "Koto", 1},
        {127, 107, "Sho", 2},
        {127, 108, "Shakuhachi", 2},
        {127, 109, "Whistle 1", 2},
        {127, 110, "Whistle 2", 1},
        {127, 111, "Bottleblow", 2},
        {127, 112, "Breathpipe", 1},
        {127, 113, "Timpani", 1},
        {127, 114, "Melodic Tom", 1},
        {127, 115, "Deep Snare", 1},
        {127, 116, "Elec Perc 1", 1},
        {127, 117, "Elec Perc 2", 1},
        {127, 118, "Taiko", 1},
        {127, 119, "Taiko Rim", 1},
        {127, 120, "Cymbal", 1},
        {127, 121, "Castanets", 1},
        {127, 122, "Triangle", 1},
        {127, 123, "Orche Hit", 1},
        {127, 124, "Telephone", 1},
        {127, 125, "Bird Tweet", 1},
        {127, 126, "One Note Jam", 1},
        {127, 127, "Water Bell", 2},
        {127, 128, "Jungle Tune", 2},
      }
    }
  }
  
  mk2.groups = {
    {
      name = "GM",
      sounds = {},
    },
    {
      name = "GS 001-008",
      sounds = {
        {8, 1, "Piano 1w", 1},
        {8, 2, "Piano 2w", 1},
        {8, 3, "Piano 3w", 1},
        {8, 4, "Honky-Tonk w", 1},
        {8, 5, "Detuned EP 1", 2},
        {8, 6, "Detuned EP 2", 2},
        {8, 7, "Coupled Hps.", 2},
        {8, 12, "Vibraphone w", 1},
        {8, 13, "Marimba w", 1},
        {8, 15, "Church Bell", 1},
        {8, 17, "Detuned Organ 1", 2},
        {8, 18, "Detuned Organ 2", 2},
        {8, 20, "Church Organ 2", 2},
        {8, 22, "Accordion It", 2},
        {8, 25, "Ukulele", 1},
        {8, 26, "12-string Guitar", 2},
        {8, 27, "Hawaiian Guitar", 1},
        {8, 28, "Chorus Guitar", 2},
        {8, 29, "Funk Guitar", 1},
        {8, 31, "Feedback Guitar", 2},
        {8, 32, "Guitar Feedback", 1},
        {1, 39, "Synth Bass 101", 1},
        {8, 39, "Synth Bass 3", 1},
        {8, 40, "Synth Bass 4", 2},
        {8, 41, "Slow Violin", 1},
        {8, 49, "Orchestra", 2},
        {8, 51, "Synth Strings 3", 2},
        {1, 58, "Trombone 2", 2},
        {1, 61, "Fr. Horn", 2},
        {8, 62, "Brass 2", 2},
        {8, 63, "Synth Brass 3", 2},
        {8, 64, "Synth Brass 4", 1},
        {1, 81, "Square", 1},
        {8, 81, "Sine Wave", 1},
        {1, 82, "Saw", 1},
        {8, 82, "Doctor Solo", 2},
        {8, 108, "Taisho Koto", 2},
        {8, 116, "Castanets", 1},
        {8, 117, "Concert BD", 1},
        {8, 118, "Melodic Tom 2", 1},
        {8, 119, "808 Tom", 1},
      }
    },
    {
      name = "GS 009-016",
      sounds = {
        {16, 1, "Piano 1d", 1},
        {16, 5, "E. Piano 1v", 2},
        {16, 6, "E. Piano 2v", 2},
        {16, 7, "Harpsi. w", 1},
        {9, 15, "Carillon", 1},
        {16, 17, "60's Organ 1", 1},
        {16, 20, "Church Organ 3", 2},
        {16, 25, "Nylon Gt.o", 2},
        {16, 26, "Mandolin", 1},
        {16, 29, "Funk Guitar 2", 1},
        {16, 40, "Rubber Bass", 2},
        {16, 63, "Analog Brass 1", 2},
        {16, 64, "Analog Brass 2", 2},
      }
    },
    {
      name = "GS 024-032",
      sounds = {
        {24, 5, "60s E. Piano", 1},
        {24, 7, "Harpsi. o", 2},
        {32, 17, "Organ 4", 2},
        {32, 18, "Organ 5", 2},
        {32, 25, "Nylon Gt.2", 1},
        {32, 53, "Choir Aahs 2", 1},
      }
    },
    {
      name = "GS SFX",
      sounds = {
        {1, 121, "Guitar Cut Noise", 1},
        {2, 121, "String Slap", 1},
        {1, 122, "Flute Key Click", 1},
        {1, 123, "Rain", 2},
        {2, 123, "Thunder", 1},
        {3, 123, "Wind", 1},
        {4, 123, "Stream", 2},
        {5, 123, "Bubble", 2},
        {1, 124, "Dog", 1},
        {2, 124, "Horse - Gallop", 1},
        {1, 125, "Telephone 2", 1},
        {2, 125, "Door Creaking", 1},
        {3, 125, "Door", 1},
        {4, 125, "Scratch", 1},
        {5, 125, "Windchime", 2},
        {1, 126, "Car - Engine", 1},
        {2, 126, "Car - Stop", 1},
        {3, 126, "Car - Pass", 1},
        {4, 126, "Car - Crash", 2},
        {5, 126, "Siren", 1},
        {6, 126, "Train", 1},
        {7, 126, "Jetplane", 2},
        {8, 126, "Starship", 2},
        {9, 126, "Burst Noise", 2},
        {1, 127, "Laughing", 1},
        {2, 127, "Screaming", 1},
        {3, 127, "Punch", 1},
        {4, 127, "Heart Beat", 1},
        {5, 127, "Foot Step", 1},
        {1, 128, "Machinegun", 1},
        {2, 128, "Lasergun", 1},
        {3, 128, "Explosion", 2},
      }
    },
    {
      name = "MT-32",
      sounds = {}
    }
  }
  
  -- Capital (GM) bank on SC-55 mkII is almost the same
  mk2.groups[1].sounds = deepcopy(mk1.groups[1].sounds)
  mk2.groups[1].sounds[19] = {0, 19, "Organ 3", 2}
  mk2.groups[1].sounds[40] = {0, 40, "Synth Bass 2", 2}
  mk2.groups[1].sounds[122] = {0, 122, "Breath Noise", 2}
  
  -- MT-32 bank is the same on both models
  mk2.groups[6].sounds = deepcopy(mk1.groups[4].sounds)

end



