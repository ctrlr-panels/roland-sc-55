module("partcfg", package.seeall)


-- Default modulators values of the part
function getDefault()
  return {
    partType = 0,
    group = 1,
    activePage = "Basic",
    mods = {
      ["part-midi-ch"] = 0,
      ["part-level"] = 64,
      ["part-pan"] = 64,
      ["part-reverb"] = 40,
      ["part-chorus"] = 0,
      ["part-key-range-low"] = 0,
      ["part-key-range-high"] = 127,
      ["part-env-attack"] = 50,
      ["part-env-decay"] = 50,
      ["part-env-release"] = 50,
      ["part-tvf-cutoff"] = 50,
      ["part-tvf-resonance"] = 50,
      ["part-vibrato-rate"] = 50,
      ["part-vibrato-depth"] = 50,
      ["part-vibrato-delay"] = 50,
      ["part-key-shift"] = 64,
      ["part-pitch-fine"] = "0",
      ["part-tuning-c"] = 64,
      ["part-tuning-cs"] = 64,
      ["part-tuning-d"] = 64,
      ["part-tuning-ds"] = 64,
      ["part-tuning-e"] = 64,
      ["part-tuning-f"] = 64,
      ["part-tuning-fs"] = 64,
      ["part-tuning-g"] = 64,
      ["part-tuning-gs"] = 64,
      ["part-tuning-a"] = 64,
      ["part-tuning-as"] = 64,
      ["part-tuning-b"] = 64,
      ["part-rx-pitch"] = 1,
      ["part-rx-ch-pressure"] = 1,
      ["part-rx-program-change"] = 1,
      ["part-rx-control-change"] = 1,
      ["part-rx-poly-pressure"] = 1,
      ["part-rx-note-message"] = 1,
      ["part-rx-rpn"] = 1,
      ["part-rx-nrpn"] = 1,
      ["part-rx-modulation"] = 1,
      ["part-rx-volume"] = 1,
      ["part-rx-panpot"] = 1,
      ["part-rx-expression"] = 1,
      ["part-rx-hold"] = 1,
      ["part-rx-portamento"] = 1,
      ["part-rx-sostenuto"] = 1,
      ["part-rx-soft"] = 1,
      ["part-cc1-ctrl-num"] = "16",
      ["part-cc2-ctrl-num"] = "17",
      ["part-velocity-sense-depth"] = "64",
      ["part-velocity-sense-offset"] = "64",
      ["part-mono-poly"] = 1,
      ["part-assign-mode"] = 1,
      ["part-pitch-ctrl-mod"] = 0,
      ["part-pitch-ctrl-bend"] = 2,
      ["part-pitch-ctrl-cc1"] = 0,
      ["part-pitch-ctrl-cc2"] = 0,
      ["part-pitch-ctrl-caf"] = 0,
      ["part-pitch-ctrl-paf"] = 0,
      ["part-cutoff-ctrl-mod"] = 0,
      ["part-cutoff-ctrl-bend"] = 0,
      ["part-cutoff-ctrl-cc1"] = 0,
      ["part-cutoff-ctrl-cc2"] = 0,
      ["part-cutoff-ctrl-caf"] = 0,
      ["part-cutoff-ctrl-paf"] = 0,
      ["part-amp-ctrl-mod"] = 0.0,
      ["part-amp-ctrl-bend"] = 0.0,
      ["part-amp-ctrl-cc1"] = 0.0,
      ["part-amp-ctrl-cc2"] = 0.0,
      ["part-amp-ctrl-caf"] = 0.0,
      ["part-amp-ctrl-paf"] = 0.0,
      ["part-lfo1-rate-mod"] = 0.0,
      ["part-lfo1-rate-bend"] = 0.0,
      ["part-lfo1-rate-cc1"] = 0.0,
      ["part-lfo1-rate-cc2"] = 0.0,
      ["part-lfo1-rate-caf"] = 0.0,
      ["part-lfo1-rate-paf"] = 0.0,
      ["part-lfo1-pitch-mod"] = 47,
      ["part-lfo1-pitch-bend"] = 0,
      ["part-lfo1-pitch-cc1"] = 0,
      ["part-lfo1-pitch-cc2"] = 0,
      ["part-lfo1-pitch-caf"] = 0,
      ["part-lfo1-pitch-paf"] = 0,
      ["part-lfo1-tvf-mod"] = 0,
      ["part-lfo1-tvf-bend"] = 0,
      ["part-lfo1-tvf-cc1"] = 0,
      ["part-lfo1-tvf-cc2"] = 0,
      ["part-lfo1-tvf-caf"] = 0,
      ["part-lfo1-tvf-paf"] = 0,
      ["part-lfo1-tva-mod"] = 0.0,
      ["part-lfo1-tva-bend"] = 0.0,
      ["part-lfo1-tva-cc1"] = 0.0,
      ["part-lfo1-tva-cc2"] = 0.0,
      ["part-lfo1-tva-caf"] = 0.0,
      ["part-lfo1-tva-paf"] = 0.0,
      ["part-lfo2-rate-mod"] = 0.0,
      ["part-lfo2-rate-bend"] = 0.0,
      ["part-lfo2-rate-cc1"] = 0.0,
      ["part-lfo2-rate-cc2"] = 0.0,
      ["part-lfo2-rate-caf"] = 0.0,
      ["part-lfo2-rate-paf"] = 0.0,
      ["part-lfo2-pitch-mod"] = 0,
      ["part-lfo2-pitch-bend"] = 0,
      ["part-lfo2-pitch-cc1"] = 0,
      ["part-lfo2-pitch-cc2"] = 0,
      ["part-lfo2-pitch-caf"] = 0,
      ["part-lfo2-pitch-paf"] = 0,
      ["part-lfo2-tvf-mod"] = 0,
      ["part-lfo2-tvf-bend"] = 0,
      ["part-lfo2-tvf-cc1"] = 0,
      ["part-lfo2-tvf-cc2"] = 0,
      ["part-lfo2-tvf-caf"] = 0,
      ["part-lfo2-tvf-paf"] = 0,
      ["part-lfo2-tva-mod"] = 0.0,
      ["part-lfo2-tva-bend"] = 0.0,
      ["part-lfo2-tva-cc1"] = 0.0,
      ["part-lfo2-tva-cc2"] = 0.0,
      ["part-lfo2-tva-caf"] = 0.0,
      ["part-lfo2-tva-paf"] = 0.0,
    },
    regularMods = {
      ["part-instrument"] = 0,
    },
    drumMods = {
    }
  }
end


-- Packs part modulator values into part config mapping
function gather(partType)
  local result = {}
  local defaultConfig = getDefault()

  if partType == nil then
    partType = 0
  end

  result["partType"] = partType
  result["activePage"] = partpages.getActive()
  result["mods"] = mods.readToTable(defaultConfig.mods)
  result["regularMods"] = mods.readToTable(defaultConfig.regularMods)
  result["drumMods"] = mods.readToTable(defaultConfig.drumMods)

  return result
end


-- Sets modulators values according to the part config
function apply(config)
  _applyTable(config.mods)
  _applyTable(config.regularMods)
  _applyTable(config.drumMods)

  parttypes.activate(config.partType)

  if config.activePage then
    partpages.activate(config.activePage)
  end
end


function _applyTable(tbl)
  for modName, value in pairs(tbl) do
    local mod = mods.get(modName)
    if mod then
      mods.setValue(mod, value)
    else
      log("Cannot apply config for non-existent modulator: " .. modName)
    end
  end
end
