module("parttypes", package.seeall)

-- Classes
btn = {}


-- If there is a drum map associated with the part
-- then return its number, else
get = function(part)
  for i, drumPart in pairs(drumParts) do
    if drumPart == part then
      return i
    end
  end
  return 0
end


-- Activate the part type button and layers
activate = function(partType)
  local mod = parttypes.btn.get(partType)
  if mod then
    btn.activate(mod)
  end
  if partType and partType > 0 then
    layers.parttype.showMap()
  else
    layers.parttype.showNormal(activePartPage)
  end
end


-- Sets current part type globals
function setGlobal(partType, partNumber)

  if not partType then
    partType = 0
  end

  local partTypeOld = get(partNumber)

  if partTypeOld and partTypeOld > 0 then
    drumParts[partTypeOld] = nil
  end
  
  if partType > 0 then
    drumParts[partType] = partNumber
  end
  
end

function sendMidi(partType, partMidiNumber)
  local data = {0x40, partMidiNumber, 0x15, partType}
  midi.sendGSSysex(data)
end


-- Gets part type button modulator by drummap
btn.get = function(drumMapNumber)
  if drumMapNumber and drumMapNumber > 0 then
    return partTypeButtons["button-part-map" .. tostring(drumMapNumber)]
  else
    return partTypeButtons["button-part-normal"]
  end
end


-- Resets all the other buttons in group
btn.activate = function(selectedButton)
  for _, button in pairs(partTypeButtons) do
    if button ~= selectedButton then
      button:getComponent():setValue(0, false)
    end
  end
  selectedButton:getComponent():setValue(1, false)
end
