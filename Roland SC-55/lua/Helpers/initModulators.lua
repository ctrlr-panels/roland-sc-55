-- This module and function will set up the modulators values
-- along with the rest of the Lua GLOBAL variables.
-- 
-- Globals defined here:
-- 
-- Variables
-- ---------
-- partPageButtonToLayerMap    Mapping of part page button mods names to the
--                             layers they trigger.
-- partPageButtons             Mapping of the part page button modulator objects
--                             to their names for fast access.
-- partPageLayerToButtonMap    The reverse mapping of `partPageButtonToLayerMap`.
-- partTuningModNames          Table of the names of part tuning page modulators.
-- partTuningScaleModulators   Table of the part page button modulator objects
--                             for fast access.
-- partTypeButtonNames         Table of the names of part type button modulators.
-- partTypeButtons             Mapping of the part type button modulator objects
--                             to their names for fast access.
-- 


partTypeButtonNames = {
  "button-part-normal",
  "button-part-map1",
  "button-part-map2",
}

partTuningModNames = {
  "part-tuning-c",
  "part-tuning-cs",
  "part-tuning-d",
  "part-tuning-ds",
  "part-tuning-e",
  "part-tuning-f",
  "part-tuning-fs",
  "part-tuning-g",
  "part-tuning-gs",
  "part-tuning-a",
  "part-tuning-as",
  "part-tuning-b",
}

partPageButtonToLayerMap = {
  ["button-part-basic"] = "Basic",
  ["button-part-tuning"] = "Tuning",
  ["button-part-midi"] = "MIDI",
  ["button-part-wheels"] = "Wheels",
  ["button-part-cc"] = "CC",
  ["button-part-aftertouch"] = "Aftertouch",
  ["button-part-drums"] = "Drums-General",
}


function initModulators()

  --display("DEBUG")

  -- Setting Device ID
  local label = mods.get("device-id")
  panel:setGlobalVariable(0, label:getComponent():getText():getIntValue())

  -- Filling the mapping with 'Part type' buttons
  if partTypeButtons == nil then
    partTypeButtons = {}
    for _, modName in ipairs(partTypeButtonNames) do
      local mod = mods.get(modName)
      if mod then
        partTypeButtons[modName] = mod
      else
        log("[ERROR] Couldn't find the modulator: " .. modName)
      end
    end
  end

  -- Filling the mapping with 'Part page' buttons
  if partPageButtons == nil then
    partPageButtons = {}
    for modName, _ in pairs(partPageButtonToLayerMap) do
      local mod = mods.get(modName)
      if mod then
        partPageButtons[modName] = mod
      else
        log("[ERROR] Couldn't find the modulator: " .. modName)
      end
    end
  end
  
  -- Filling the reverse map of part page layers to buttons
  _G.partPageLayerToButtonMap = {}
  for modName, layer in pairs(partPageButtonToLayerMap) do
    partPageLayerToButtonMap[layer] = modName
  end

  -- Filling the table with part scale tuning modulators for fast access
  _G.partTuningScaleModulators = {}
  for _, modName in ipairs(partTuningModNames) do
    local mod = mods.get(modName)
    if mod then
      table.insert(partTuningScaleModulators, mod)
    else
      log("[ERROR] Couldn't find the modulator: " .. modName)
    end
  end
  
  -- Setting up current part
  if (currentPart > 0 and currentPart <= PARTS_COUNT) then
    parts.activate(currentPart)

    local currentButton = mods.get(
      "button-part-" .. tostring(currentPart)
    )
    currentButton:getComponent():setValue(1, true)
  end
  
  -- Activate the part type button and layers
  local partType = parttypes.get(currentPart)
  parttypes.activate(partType)

  -- Activate the part page layer
  local activePartPage = partConfigs[currentPart].activePage
  partpages.activate(activePartPage)

  -- NB: the following should be the last line of function!
  isPanelInitialized = true
  
end