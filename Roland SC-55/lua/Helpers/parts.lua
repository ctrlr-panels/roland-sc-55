module("parts", package.seeall)

btn = {}
mod = {}


-- Does all the job of changing the part
function activate(newPart)

  local isPartChanged = (newPart ~= currentPart)

  -- Setting caption on Part panel
  mods.setValue(
    "label-part-caption",
    "PART " .. string.format("%02d", newPart)
  )

  -- Show Part panel
  layers.top.showPart()

  setGlobal(newPart)

  if isPartChanged then
    -- Setting modulators values on part panel
    partcfg.apply(partConfigs[newPart])
  end

end


-- Sets current part globals
function setGlobal(num)
  if num and num > 0 and num <= PARTS_COUNT then
	_G.currentPart = num
    panel:setGlobalVariable(1, midiNumber(currentPart) + 16)  -- 0x1N
    panel:setGlobalVariable(2, midiNumber(currentPart) + 32)  -- 0x2N
  else
    msg = "Wrong current part given (" .. tostring(num) .. ")"
    error(msg)
  end
end


-- Calculates the midi representation of part (i.e 10 = 0, 1 = 1, 16 = 15)
function midiNumber(part)
  if part == 10 then
    return 0
  elseif part > 0 and part < 10 then
    return part
  elseif part > 10 and part <= PARTS_COUNT then
    return part - 1
  else
    error("Wrong part number given (" .. tostring(part) .. ")")
  end
end


local controlGroupMidiNumbers = {
  mod = 0,
  bend = 16,
  caf = 32,
  paf = 48,
  cc1 = 64,
  cc2 = 80,
}

function controlGroupMidiNumber(groupName)
  return controlGroupMidiNumbers[groupName]
end


-- Gets part number of a button
function btn.getPartNo(mod)
  local modName = mod:getProperty("name")
  local partNumber = string.gsub(modName, "^.+\-(%d+)$", "%1")
  return tonumber(partNumber)
end


-- Resets the part buttons
function btn.reset()
  for i=1, PARTS_COUNT do
    local mod = mods.get("button-part-" .. tostring(i))
    if mod:getValue() > 0 then
      mod:getComponent():setValue(0, false)
    end
  end
end


-- Gets control group name
-- (mod/bend/cc1/cc2/caf/paf)
function mod.getControlGroupName(mod)
  local modName = mod:getOwner():getName():unquoted()
  return string.gsub(modName, "^.+\-(.+)$", "%1")
end
