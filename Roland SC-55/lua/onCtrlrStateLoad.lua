--
-- Called when data is restored
--
onCtrlrStateLoad = function(--[[ ValueTree --]] stateData)
  if stateData:hasProperty("currentPart") then
    currentPart = tonumber(stateData:getProperty("currentPart"))
  end
  if stateData:hasProperty("partConfigs") then
    partConfigs = json.decode(stateData:getProperty("partConfigs"))
  end
end