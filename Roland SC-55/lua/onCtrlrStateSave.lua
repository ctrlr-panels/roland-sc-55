--
-- Called when data needs saving
--
onCtrlrStateSave = function(--[[ ValueTree --]]stateData)
  stateData:setProperty("currentPart", currentPart, nil)
  stateData:setProperty(
    "partConfigs",
    json.encode(partConfigs),
    nil
  )
end