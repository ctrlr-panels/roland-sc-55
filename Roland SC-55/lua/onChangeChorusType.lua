--
-- Called when a global Chorus Type changes
--
-- In SC-55 all the chorus parameters are being reset after type change
-- 
-- @mod      chorus modulator
-- @value    new numeric value of the modulator
--

CHORUS_MODULATOR_NAMES = {
  "chorus-pre-lpf",
  "chorus-feedback",
  "chorus-delay",
  "chorus-rate",
  "chorus-depth",
  "chorus-send-to-reverb",
}

onChangeChorusType = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)
  for i = 1, #CHORUS_MODULATOR_NAMES do
    mods.reset(CHORUS_MODULATOR_NAMES[i])
  end
end