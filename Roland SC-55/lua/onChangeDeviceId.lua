--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangeDeviceId = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local value = convertToNumber(newContent, 16)

  -- Validation
  if value < 0 then
    value = 0
  elseif value > 31 then
    value = 31
  end

  -- Setting correct value into label
  label:setComponentText(tostring(value))
  
  panel:setGlobalVariable(0, value)
end