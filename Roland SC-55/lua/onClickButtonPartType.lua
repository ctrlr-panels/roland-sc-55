--
-- Called when a modulator value changes
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--


onClickButtonPartType = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)

  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local partTypeOld = parttypes.get(currentPart)
  local partTypeNew = 0

  local typeName = string.sub(mod:getName():unquoted(), 13)

  if typeName == "normal" then

    parttypes.btn.activate(mod)

    -- If double-clicked just highlight the button again
    if not partTypeOld or partTypeOld == 0 then
      return
    end

    layers.parttype.showNormal(activePartPage)
    
  else

    partTypeNew = tonumber(string.sub(typeName, -1))

    -- If double-clicked just highlight the button again
    if partTypeNew == partTypeOld then
      parttypes.btn.activate(mod)
      return
    end

    -- If this type of map has already been assigned, then
    -- highlight the previous button
    if drumParts[partTypeNew] then
      cancelButtonPartTypeClick(mod, partTypeOld)
      return
    end

    parttypes.btn.activate(mod)
    layers.parttype.showMap()

  end
  
  parttypes.setGlobal(partTypeNew, currentPart)
  
  -- TODO: maybe add some indication
  
  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({"button-part-normal", "button-part-map1", "button-part-map2"}) then
    return
  end
  
  parttypes.sendMidi(partTypeNew, panel:getGlobalVariable(1))

end


-- Returns the focus back on the previous button
-- (judging by drum map numbers)
cancelButtonPartTypeClick = function(mod, partTypeOld)
  mod:getComponent():setValue(0, false)
  parttypes.btn.activate(parttypes.btn.get(partTypeOld))
end
