--
-- Called when the contents of a Label are changed
-- @label
-- @newContent    a string that the label now contains
--

onChangePartPitchControl = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local controlGroup = parts.mod.getControlGroupName(label)

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  value = math.floor(value)
  if controlGroup == "bend" then
    value = fitInRange(value, 0, 24)
  else
    value = fitInRange(value, -24, 24)
  end

  -- Setting correct value into label
  mods.setValue(label, string.format("%d", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua({
    "part-pitch-ctrl-mod",
    "part-pitch-ctrl-bend",
    "part-pitch-ctrl-cc1",
    "part-pitch-ctrl-cc2",
    "part-pitch-ctrl-caf",
    "part-pitch-ctrl-paf",
  }) then
    return
  end

  local midiNumber = parts.controlGroupMidiNumber(controlGroup)
  local midiValue = value + 64

  local data = {
    0x40,
    panel:getGlobalVariable(2),
    midiNumber + 0,
    midiValue,
  }
  midi.sendGSSysex(data)

end