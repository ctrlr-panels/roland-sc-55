--
-- Called when the CC1 Controller Number is changed
-- @label       modulator (label)
-- @newContent  a new value (string)
--
onChangePartCC1CtrlNum = function(--[[ CtrlrLabel --]] label, --[[ String --]] newContent)
  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  local value = convertToNumber(newContent, 0.0)

  -- Validation
  if value < 0 then
    value = 0
  elseif value > 127 then
    value = 127
  end

  -- Setting correct value into label
  label:setComponentText(string.format("%d", value))

  -- Don't need to send midi if triggered from code
  if isTriggeredFromLua("part-cc1-ctrl-num") then
    return
  end

  local data = {
    0x40,
    panel:getGlobalVariable(1),
    0x1f,
    value,
  }
  midi.sendGSSysex(data)
end