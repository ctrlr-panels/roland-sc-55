--
-- Called when a modulator value changes
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--
onChangePartTuningScale = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)
  -- Skipping if called from code
  if not isPanelReady() or source == 5 then
    return
  end

  local data = {0x40, panel:getGlobalVariable(1), 64}
  for _, mod in ipairs(partTuningScaleModulators) do
    table.insert(data, mod:getValue())
  end
  midi.sendGSSysex(data)

end