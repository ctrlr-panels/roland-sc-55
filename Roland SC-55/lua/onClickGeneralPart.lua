--
-- Called when a click on "General" part button is made
-- @mod   http://ctrlr.org/api/class_ctrlr_modulator.html
-- @value    new numeric value of the modulator
--
onClickGeneralPart = function(--[[ CtrlrModulator --]] mod, --[[ number --]] value, --[[ number --]] source)

  -- Eliminate false triggers
  if not isPanelReady() then
    return
  end

  -- TODO: needs to edit when the ordering of the parts is changed
  layers.top.showGeneral()
  mod:getComponent():setValue(1, false)

end
